import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import '@testing-library/react/cleanup-after-each';

import { render, fireEvent } from '@testing-library/react';

import axios from 'axios';
import 'babel-polyfill';

import OrderComponent from '../Order';

jest.mock('axios'); // Mock axios模块

test('Order组件显示异步调用订单数据', async () => {
  // <--start
  // TODO 4: 给出正确的测试
  // setup组件
  const { findByTestId, getByLabelText } = render(<OrderComponent />);
  // Mock数据请求
  axios.get.mockResolvedValueOnce({
    data: { status: '已完成' }
  });
  // 触发事件
  fireEvent.click(getByLabelText('submit-button'));
  // 给出断言
  const currentStatus = await findByTestId('status');
  expect(currentStatus.textContent).toBe('已完成');

  // --end->
});
